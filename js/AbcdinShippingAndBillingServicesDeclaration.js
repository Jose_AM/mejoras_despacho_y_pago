//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2008, 2014 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

//
//

/** 
 * @fileOverview This file declares services that are used in the store check-out pages.
 */

dojo.require("wc.service.common");


/** 
 * This file declares services that are used in the store check-out pages.
 * 
 * @class This SBServicesDeclarationJS class defines all the variables and functions for the page(s) that use shipping and billing functionality in the store.
 *
 */
AbcdinSBServicesDeclarationJS = {
	/** 
	 * This variable stores the ID of the language that the store is currently using.
	 *
	 * @private
	 */
	langId: "-1",
	
	/** 
	 * This variable stores the ID of the current store.
	 *
	 * @private
	 */
	storeId: "",
	
	/** 
	 * This variable stores the ID of the catalog that is used in the store.
	 *
	 * @private
	 */
	catalogId: "",
	
	/** 
	 * This variable stores the value that indicates if the current check-out shopping flow is in AJAX mode or not. 
	 *
	 * @private
	 */
	ajaxCheckOut: true,

	/** 
	 * This variable stores the true/false value that indicates if the 'SinglePageCheckout' feature is enabled/disabled.
	 * When it is true, both shipping and billing information are captured in a single page. If it is false, checkout will
	 * be a two step process where shipping is captured in first step and billing in second step.
	 * It is set to true by default.
	 * 
	 * @private
	 */
	singlePageCheckout:true,
	
	/**
	 * This variable stores the value that indicates if an <code>OrderProcessServiceOrderPrepare</code> service call is required.
	 *
	 * @private
	 */
	orderPrepare: false,
	
	jsonInfoDespachoOrden : "",
	
	setJsonInfoDespachoOrden:function(jsonInfoDespachoOrden){
		this.jsonInfoDespachoOrden = jsonInfoDespachoOrden;
	},
	
	getJsonInfoDespachoOrden:function(){
		return this.jsonInfoDespachoOrden;
	},
	
	comboMediosPagos : false,
	
	setComboMediosPagos:function(comboMediosPagos){
		this.comboMediosPagos = comboMediosPagos;
	},
	
	getComboMediosPagos:function(){
		return this.comboMediosPagos;
	},
	
	/**
 	 * Sets the common parameters for this instance.
 	 *
 	 * @param {int} langId The ID of the language used in the store.
 	 * @param {int} storeId The ID of the store.
 	 * @param {catalogId} catalogId The ID of the catalog used in the store.
	 */
	setCommonParameters:function(langId,storeId,catalogId){
			this.langId = langId;
			this.storeId = storeId;
			this.catalogId = catalogId;
	},
	
	/**
 	 * Sets the flag which indicates whether the 'AjaxCheckout' feature is enabled or not.
 	 *
 	 * @param {boolean} checkOutType A value that indicates if the 'AjaxCheckout' feature is enabled.
	 */
	setAjaxCheckOut:function(checkOutType){
		this.ajaxCheckOut = checkOutType;
	},
	
	/**
 	 * Returns the flag that indicates whether 'AjaxCheckout' feature is enabled or not.
 	 *
 	 * @returns {boolean} ajaxCheckOut A value that indicates if the 'AjaxCheckout' feature is enabled.
	 */
	isAjaxCheckOut:function(){
		return this.ajaxCheckOut;
	},

	/**
	 * Sets the SinglePageCheckout variable to indicate if the 'SinglePageCheckout' feature is enabled or disabled.
	 * 
	 * @param {Boolean} singlePageCheckout. A true/false value that indicates if the 'SinglePageCheckout' feature is enabled.
	 *
	 * @see CheckoutHelperJS.isSinglePageCheckout
	 */
	setSinglePageCheckout:function(singlePageCheckout){
		this.singlePageCheckout = singlePageCheckout;
	},
	
	
	/**
	 * Returns the singlePageCheckout variable that indicates if the 'SinglePageCheckout' feature is enabled/disabled.
	 * 
	 * @returns {Boolean} singlePageCheckout A true/false value that indicates if the 'SinglePageCheckout' feature is
	 * enabled/disabled.
	 *
	 * @see CheckoutHelperJS.setSinglePageCheckout
	 */
	isSinglePageCheckout:function(){
		return this.singlePageCheckout;
	}

}

/**
 * This service redirect to shipping and billing page or my account page. It depends if principal address is complete *
 * @constructor
 */
wc.service.declare({
	id: "AjaxValidateAddressComplete",
	actionId: "AjaxValidateAddressComplete",
	url: "AjaxValidateAddressComplete",
	formId: ""
		
	 /**
     * hides all the messages and the progress bar
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
	,successHandler: function(serviceResponse) {
		cursor_clear();
		
		/*
		infoAsJSON = serviceResponse.dataResponse;
		var form = document.getElementById("AjaxLogon");
		
		if (infoAsJSON.success){			
			form.URL.value = ShipmodeSelectionExtJS.completeOrderMoveURLToShippingPage;
		}
		else{
			form.URL.value = ShipmodeSelectionExtJS.completeOrderMoveURLToMyAccountPage;			
		}
		if (infoAsJSON.errorMessage){	
			MessageHelper.displayErrorMessage(infoAsJSON.errorMessage);
		}		
		document.location.href = ShipmodeSelectionExtJS.logonURL;
		*/
		var comboDespacho = document.getElementById("singleShippingMode");	
		comboDespacho.disabled = true;
		document.getElementById("isCompleteAddress").value = 1;
		var singleShipmentAddress = dojo.byId('singleShipmentAddress');
		CheckoutHelperJS.displayAddressDetails(singleShipmentAddress.value,'Shipping');
		CheckoutHelperJS.updateAddressForAllItems(singleShipmentAddress,serviceResponse.refURL);
		CheckoutHelperJS.showHideEditAddressLink(singleShipmentAddress,serviceResponse.orderId);
	}
	/**
	 * Displays the error message returned with the service response and hides the progress bar.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,failureHandler: function(serviceResponse) {

		var existeRUT = document.getElementById('existeRUT');
		if(existeRUT== null || existeRUT!=null && existeRUT.value != 'false'){
				cursor_clear();
		}
		document.getElementById("isCompleteAddress").value = 0;
		
		CheckoutHelperJS.editAddress('singleShipmentAddress','-1',serviceResponse.profileshipping,serviceResponse.profilebilling);
		CheckoutHelperJS.setLastAddressLinkIdToFocus('WC_ShippingAddressSelectSingle_link_1');
//		if (serviceResponse.errorMessage) {
//			MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
//		} else {
//			if (serviceResponse.errorMessageKey) {
//				MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
//			}
//		}
		if(existeRUT!=null && existeRUT.value == 'false'){
				cursor_clear();
		}
	}
})

/**
 * This service redirect to shipping and billing page or my account page. It depends if principal address is complete *
 * @constructor
 */
wc.service.declare({
	id: "ActualizarCamposEnCompras",
	actionId: "ActualizarCamposEnCompras",
	url: "AjaxAbcdinActualizadDatosEnCompras",
	formId: ""
		
	 /**
     * hides all the messages and the progress bar
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
	,successHandler: function(serviceResponse) {
		
		console.debug("AjaxAbcdinActualizadDatosEnCompras.successHandler BEGIN");
		
		var infoAsJSON = serviceResponse.dataResponse;		
		
		if (infoAsJSON.success){		
			var divOcultar = infoAsJSON.divOcultar;		
			console.debug("divOcultar:" + divOcultar);
			//dojo.byId(divOcultar).style.display = "none";

			if (divOcultar == 'divIngresarRUT'){
				dojo.byId('botonActualizarRUT').style.display = "none";
				dojo.byId('userField1_1').disabled = true;
				dojo.byId('userField1_2').disabled = true;
				
				var existeRUT = document.getElementById('existeRUT');
				existeRUT.value = 'true';
				
				var comboPayment = document.getElementById("payMethodId_1");	
				comboPayment.disabled = false;
				
				//var comboAddressTemp = document.getElementById("singleShipmentAddress");	
				//comboAddressTemp.disabled = false;
				
				//var divShippingButtonIdTemp = document.getElementById("divShippingButtonId");	
				//divShippingButtonIdTemp.style.display = "block";
			}
			else{
				dojo.byId('botonActualizarEmail').style.display = "none";
				dojo.byId('emailCliente').disabled = true;
			}
			
			
			
			
			
			//console.debug("ocultarTodo:" + infoAsJSON.ocultarTodo);
			//if(infoAsJSON.ocultarTodo){				
			//	dojo.byId('divPrincipalCompletarDatos').style.display = "none";				
			//}
		}
		
		/*
		var popupFlag = serviceResponse.flagCampo;
		if (popupFlag == '0')
			dijit.byId('quickInfoRUT').hide();
		else
			dijit.byId('quickInfoEmail').hide();
		*/
		MessageHelper.hideAndClearMessage();
		cursor_clear();
		var completedAddress = document.getElementById("isCompleteAddress");
		if(completedAddress!=null && completedAddress.value==0 && document.getElementById("singleShipmentAddress")!=null){
				var currentaddress=document.getElementById("singleShipmentAddress").value;
				document.getElementById("singleShipmentAddress").onchange();
		}

	}
	/**
	 * Displays the error message returned with the service response and hides the progress bar.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,failureHandler: function(serviceResponse) {
		if (serviceResponse.errorMessage) {
			MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
		} else {
			if (serviceResponse.errorMessageKey) {
				MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
			}
		}
		cursor_clear();
	}
})


/**
 * This service redirect to shipping and billing page or my account page. It depends if principal address is complete *
 * @constructor
 */
wc.service.declare({
	id: "AjaxValidateAddressComplete1",
	actionId: "AjaxValidateAddressComplete",
	url: "AjaxValidarDireccionCompleta",
	formId: ""
		
	 /**
     * hides all the messages and the progress bar
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
	,successHandler: function(serviceResponse) {
		
		console.debug("AjaxValidateAddressComplete1.successHandler BEGIN");
		
		infoAsJSON = serviceResponse.dataResponse;
		
		var form = document.getElementById("AjaxLogon");
		var urlRedirect = "";
		
		if (infoAsJSON.success){			
			urlRedirect = ShipmodeSelectionExtJS.completeOrderMoveURLToShippingPage;
			
		}
		else{
			urlRedirect = ShipmodeSelectionExtJS.completeOrderMoveURLToMyAccountPage;			
		}
		
		console.debug("urlRedirect:" + urlRedirect);
		
		if (infoAsJSON.errorMessage){	
			MessageHelper.displayErrorMessage(infoAsJSON.errorMessage);
		}
		
		document.location.href = urlRedirect;
		
		cursor_clear();
		
		
	}
	/**
	 * Displays the error message returned with the service response and hides the progress bar.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,failureHandler: function(serviceResponse) {
		if (serviceResponse.errorMessage) {
			MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
		} else {
			if (serviceResponse.errorMessageKey) {
				MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
			}
		}
		cursor_clear();
	}
})

/**
 * This service updates an order item in the shopping cart.
 * A message is displayed after the service call.
 * @constructor
 */
wc.service.declare({
	id: "AjaxUpdateOrderFechaDespacho",
	actionId: "AjaxUpdateOrderFechaDespacho",
	url: "AjaxObtenerFechaDespachoEnCompras",
	formId: ""
		
	 /**
     * hides all the messages and the progress bar
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
	,successHandler: function(serviceResponse) {
		
		console.debug("AjaxUpdateOrderFechaDespacho.successHandler BEGIN");
		
		infoAsJSON = serviceResponse.dataResponse;

		document.getElementById("payMethodId_1").disabled = true;
		if (infoAsJSON.success){

			var calendarioPrincipalFormato = document.getElementById('requestedShippingDate');
			calendarioPrincipalFormato.value = infoAsJSON.fechaUnificada;

			isLoadPage = false;
			$('.detailShipping').show();
			cursor_clear();
			
			AbcdinSBServicesDeclarationJS.setJsonInfoDespachoOrden(infoAsJSON);
			//if (infoAsJSON.seleccionoCalendario)
			//	CheckoutHelperJS.invocoServicioCalculadora = true;
			
			console.debug("successHandler infoAsJSON.flagUnificada:" + infoAsJSON.flagUnificada);
			if(infoAsJSON.existeOrdenItemSeleccionada){
				console.debug("successHandler infoAsJSON.orderItemSeleccionada:" + infoAsJSON.orderItemSeleccionada);
				
				var calendarioOrderItem = document.getElementById('MS_requestedShippingDate_'+ infoAsJSON.orderItemSeleccionada);
				var totalItems = infoAsJSON.totalItem;
				var despachoInfoUnificadoJson = infoAsJSON.infoDespachoOrderItem;
				for(var j=0; j < totalItems; j++){
					var objInfoDespacho = despachoInfoUnificadoJson[j];
					if(objInfoDespacho.orderItemId == infoAsJSON.orderItemSeleccionada){
						calendarioOrderItem.value = objInfoDespacho.fechaOrdenItemDespacho;
					}
				}
				
				var objectShippingDate = dijit.byId("MS_requestedShippingDate_" + infoAsJSON.orderItemSeleccionada);				
				setCurrentId(objectShippingDate.id);				
				CheckoutHelperJS.updateRequestedShipDateForThisItem(objectShippingDate,infoAsJSON.orderItemSeleccionada);
			}else{				
				if(infoAsJSON.seleccionoCalendario){
					CheckoutHelperJS.invocoServicioCalculadora = true;
				}
				
				
				var objectShippingDate = dijit.byId("requestedShippingDate");				
				setCurrentId(objectShippingDate.id); 
				CheckoutHelperJS.updateRequestedShipDate(objectShippingDate);
			}
			
			document.getElementById("payMethodId_1").disabled = false;
			jQuery("#eCommerceField4").removeAttr("disabled");
		}
		else{
			
			var isOrderItemSeleccionada = infoAsJSON.isOrderItemSeleccionado;
			if(isOrderItemSeleccionada){
				var calendarioOrderItem = document.getElementById('MS_requestedShippingDate_'+ infoAsJSON.orderItemIdSeleccionada);
				calendarioOrderItem.value = infoAsJSON.ultimaFechaSeleccionada;
			}
			else{
				var calendarioPrincipalFormato = document.getElementById('requestedShippingDate');
				calendarioPrincipalFormato.value = infoAsJSON.ultimaFechaSeleccionada;
			}
			
			var comboDespacho = document.getElementById("singleShippingMode");
			comboDespacho.focus();
			
			CheckoutHelperJS.invocoServicioCalculadora = true;
			
			MessageHelper.displayErrorMessage(infoAsJSON.message);

			isLoadPage = false;
			$('.detailShipping').hide();
			cursor_clear();
		}
		
		
		
	}
	/**
	 * Displays the error message returned with the service response and hides the progress bar.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,failureHandler: function(serviceResponse) {
		if (serviceResponse.errorMessage) {
			MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
		} else {
			if (serviceResponse.errorMessageKey) {
				MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
			}
		}
		
		document.getElementById("payMethodId_1").disabled = true;
		isLoadPage = false;
		cursor_clear();
	}
})

/**
 * Declares an AJAX service that adds an item to the wish list and delete an item from the shopping cart.
 * @constructor 
 */
wc.service.declare({
	id: "AjaxAbcdinActualizarDocumento",
	actionId: "AjaxAbcdinActualizarDocumento",
	url: "ActualizarTipoDocumento",
	formId: ""

	/**
	 * Hides the progress bar and deletes the order item from the shopping cart.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,successHandler: function(serviceResponse) {
		MessageHelper.hideAndClearMessage();
		cursor_clear();
		
		var existeRUT = document.getElementById('existeRUT').value;	
		if(existeRUT == 'false'){
			MessageHelper.displayErrorMessage("Recuerda ingresar tu Rut y haz clic en el bot&oacute;n guardar.");	
		}
				
		var tipoDocumento = document.getElementById("eCommerceField4").value;
		
		if(tipoDocumento == "0")		
			document.getElementById("payMethodId_1").disabled = false;
		else
			document.getElementById("payMethodId_1").disabled = true;
	}
	
	/**
	 * Displays the error message returned with the service response and hides the progress bar.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,failureHandler: function(serviceResponse) {
		if (serviceResponse.errorMessage) {
			MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
		} else {
			if (serviceResponse.errorMessageKey) {
				MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
			}
		}
		cursor_clear();
	}

}),

/**
 * This service updates an order item in the shopping cart.
 * A message is displayed after the service call.
 * @constructor
 */
wc.service.declare({
	id: "AjaxAbcdinCalcularCuota",
	actionId: "AjaxAbcdinCalcularCuota",
	url: "AjaxAbcdinCalcularCuotaEnCompras",
	formId: ""
		
	 /**
     * hides all the messages and the progress bar
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
	,successHandler: function(serviceResponse) {		
		
		console.debug("AjaxAbcdinCalcularCuota.successHandler BEGIN");
		var form = document.getElementById('paymentSection1');
		CheckoutHelperJS.habilitarCampos(form,true);
		infoAsJSON = serviceResponse.dataResponse;
		
		if (infoAsJSON.success){
			dojo.byId("FinancialCalculator1_text").innerHTML = "Valor de la cuota: ";
		    dojo.byId("FinancialCalculator1_valueFee").innerHTML = infoAsJSON.valorCuotaFormat;
		    if (infoAsJSON.showValorCae) {
		    	dojo.byId("areaValorCae").style.display = "block";
		    	dojo.byId("valorCae").innerHTML = infoAsJSON.valorCaeFormat;
		    }
		    if (infoAsJSON.showTotalCredito) {
		    	dojo.byId("areaTotalCredito").style.display = "block";
		    	dojo.byId("totalCredito").innerHTML = infoAsJSON.totalCreditoFormat;
		    }
		    dojo.byId("MedioPago").value = infoAsJSON.MedioPago;
		    dojo.byId("TasaInteres").value = infoAsJSON.TasaInteres;
		    dojo.byId("DescuentoPrimeraCompra").value = infoAsJSON.DescuentoPrimeraCompra;
		    dojo.byId("EmpresaId").value = infoAsJSON.EmpresaId;
		    dojo.byId("Cuenta").value = infoAsJSON.Cuenta;
		    dojo.byId("Tarjeta").value = infoAsJSON.Tarjeta;
		    dojo.byId("NumCuotas").value = infoAsJSON.NumCuotas;
		    dojo.byId("MontoCuotas").value = infoAsJSON.MontoCuotas;
		    dojo.byId("FechaPrimerVencimiento").value = infoAsJSON.FechaPrimerVencimiento;
		    dojo.byId("DiasDesfase").value = infoAsJSON.DiasDesfase;
		    dojo.byId("ValorRescate").value = infoAsJSON.ValorRescate;
		    dojo.byId("IndicadorValorCae").value = infoAsJSON.IndicadorValorCae;
		    dojo.byId("IndicadorTotalCredito").value = infoAsJSON.IndicadorTotalCredito;
		    dojo.byId("ValorCae").value = infoAsJSON.ValorCae;
		    dojo.byId("TotalCredito").value = infoAsJSON.TotalCredito;
		    
		    //etapia: nuevos tags para el boletero
		    dojo.byId("OrigenDatos").value = infoAsJSON.OrigenDatos;
		    dojo.byId("ValorProcesoFlujo").value = infoAsJSON.ValorProcesoFlujo;
		    //etapia: nuevos tags para el boletero
		     
		    dojo.byId("shippingBillingPageNext").style.display = '';
		    
		     //var selNumberFees = form['numberFees'];
		     //selNumberFees.options[0].disabled = true;
		    
		    jQuery('#paymentButtonAbcVisa').show();
		}
		else{
			//dojo.byId("FinancialCalculator1_text").innerHTML = 'Mensaje: ';
			//dojo.byId("FinancialCalculator1_valueFee").innerHTML = infoAsJSON.mensajeError;
		    //dojo.byId("shippingBillingPageNext").style.display = 'none';
		    
			dojo.byId("tarjetaCliente").value = "";
			dojo.byId("firstPay").selectedIndex = 0;
			dojo.byId("numberFees").selectedIndex = 0;
			
			dojo.byId("errorTarjetaAbcdin").innerHTML = "<red>" + infoAsJSON.mensajeError + "</red>"; 
		    //MessageHelper.displayErrorMessage(infoAsJSON.mensajeError);

		    //var selNumberFees = form['numberFees'];
		    //selNumberFees.options[0].disabled = false;
		    //selNumberFees.options[0].selected = true;
		}
		
		cursor_clear();
		
	}
	/**
	 * Displays the error message returned with the service response and hides the progress bar.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,failureHandler: function(serviceResponse) {
		habilitarCampos(form,true);
		if (serviceResponse.errorMessage) {		   			
			MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
		} else {
			if (serviceResponse.errorMessageKey) {
				MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
			}
		}
		cursor_clear();
	}
})

/**
 * This service updates an order item in the shopping cart.
 * A message is displayed after the service call.
 * @constructor
 */
wc.service.declare({
	id: "AjaxAbcdinUpdateOrderFieldsPayments",
	actionId: "AjaxAbcdinUpdateOrderFieldsPayments",
	url: "AjaxAbcdinUpdateOrderFieldsPayments",
	formId: ""
		
	 /**
     * hides all the messages and the progress bar
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
	,successHandler: function(serviceResponse) {		
		
		console.debug("AjaxAbcdinCalcularCuota.successHandler BEGIN");
		
		if (serviceResponse.dataResponse.success){
			CheckoutPayments.getTotalInJSON("OrderPrepare", 'PaymentForm', false);
		}else{
						
			CheckoutPayments.setInitializedCheckoutProcess(false);
		}
		
		cursor_clear();
		
	}
	/**
	 * Displays the error message returned with the service response and hides the progress bar.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,failureHandler: function(serviceResponse) {
		
		CheckoutPayments.setInitializedCheckoutProcess(false);

		if (serviceResponse.errorMessage) {		   			
			MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
		} else {
			if (serviceResponse.errorMessageKey) {
				MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
			}
		}
		cursor_clear();
	}
})

/**
 * This service removes the previous PI of an order item in the shopping cart.
 * A message is displayed after the service call.
 * @constructor
 */	
wc.service.declare({
	id: "AjaxAbcdinRESTOrderPIDelete",
	actionId: "AjaxAbcdinRESTOrderPIDelete",
	url: "AjaxAbcdinRemovePaymentInstructions",
	formId: "",
	nextURL : ""
		
	 /**
     * hides all the messages and the progress bar
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
	,successHandler: function(serviceResponse) {		
					
		cursor_clear();
		
		document.location.href = this.nextURL;
	}
	/**
	 * Displays the error message returned with the service response and hides the progress bar.
	 *
	 * @param (object) serviceResponse The service response object, which is the JSON object returned by the service invocation.
	 */
	,failureHandler: function(serviceResponse) {
		if (serviceResponse.errorMessage) {		   			
			MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
		} else {
			if (serviceResponse.errorMessageKey) {
				MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
			}
		}
		cursor_clear();
	}
})

wc.service.declare({
	id: "AjaxEnablePaymentButton",
	actionId: "AjaxEnablePaymentButton",
	url: getAbsoluteURL() + "TarjetaAbiertaCmd",
	formId: ""

	,successHandler: function(serviceResponse) {
		infoAsJSON = serviceResponse.dataResponse;
		
		if (infoAsJSON.success){
			dojo.byId("valorCuota").innerHTML = infoAsJSON.valorCuotaFormat;
			dojo.byId("valorCae").innerHTML = infoAsJSON.valorCaeFormat;
			dojo.byId("totalCredito").innerHTML = infoAsJSON.totalCreditoFormat;
			
			dojo.byId("MedioPago").value = infoAsJSON.MedioPago;
		    dojo.byId("TasaInteres").value = infoAsJSON.TasaInteres;
		    dojo.byId("EmpresaId").value = infoAsJSON.EmpresaId;
		    dojo.byId("DescuentoPrimeraCompra").value = infoAsJSON.DescuentoPrimeraCompra;
		    dojo.byId("Cuenta").value = infoAsJSON.Cuenta;
		    dojo.byId("Tarjeta").value = infoAsJSON.Tarjeta;
		    dojo.byId("NumCuotas").value = infoAsJSON.NumCuotas;
		    dojo.byId("MontoCuotas").value = infoAsJSON.MontoCuotas;
		    dojo.byId("FechaPrimerVencimiento").value = infoAsJSON.FechaPrimerVencimiento;
		    dojo.byId("DiasDesfase").value = infoAsJSON.DiasDesfase;
		    dojo.byId("ValorRescate").value = infoAsJSON.ValorRescate;
		    dojo.byId("IndicadorValorCae").value = infoAsJSON.IndicadorValorCae;
		    dojo.byId("IndicadorTotalCredito").value = infoAsJSON.IndicadorTotalCredito;
		    dojo.byId("ValorCae").value = infoAsJSON.ValorCae;
		    dojo.byId("TotalCredito").value = infoAsJSON.TotalCredito;
		    
		    //etapia: nuevos tags para el boletero
		    dojo.byId("OrigenDatos").value = infoAsJSON.OrigenDatos;
		    dojo.byId("ValorProcesoFlujo").value = infoAsJSON.ValorProcesoFlujo;
		    //etapia: nuevos tags para el boletero
		    
			$('#paymentButtonAbcVisa').show();
			$('#resultSimulacion').show();
		}
		else{
			dojo.byId("errorTarjetaAbcdin").innerHTML = "<red>" + infoAsJSON.mensajeError + "</red>";
			CheckoutHelperJS.resetearCamposAbcdinVisa();
			$('#resultSimulacion').hide();
			$('#paymentButtonAbcVisa').hide();
		}
		cursor_clear();
	}

	,failureHandler: function(serviceResponse) {
		cursor_clear();
	}
})